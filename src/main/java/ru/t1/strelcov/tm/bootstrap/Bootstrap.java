package ru.t1.strelcov.tm.bootstrap;

import ru.t1.strelcov.tm.api.repository.*;
import ru.t1.strelcov.tm.api.service.*;
import ru.t1.strelcov.tm.command.AbstractCommand;
import ru.t1.strelcov.tm.command.system.*;
import ru.t1.strelcov.tm.command.task.*;
import ru.t1.strelcov.tm.command.project.*;
import ru.t1.strelcov.tm.command.user.*;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.repository.*;
import ru.t1.strelcov.tm.service.*;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        register(new DisplayHelpCommand());
        register(new DisplayAboutCommand());
        register(new DisplayVersionCommand());
        register(new DisplaySystemInfoCommand());
        register(new DisplayArguments());
        register(new DisplayCommands());
        register(new ExitCommand());
        register(new ProjectClearCommand());
        register(new ProjectCreateCommand());
        register(new ProjectListCommand());
        register(new ProjectCompleteByIdCommand());
        register(new ProjectCompleteByIndexCommand());
        register(new ProjectCompleteByNameCommand());
        register(new ProjectRemoveByIdCommand());
        register(new ProjectRemoveByIndexCommand());
        register(new ProjectRemoveByNameCommand());
        register(new ProjectFindByIndexCommand());
        register(new ProjectFindByIdCommand());
        register(new ProjectFindByNameCommand());
        register(new ProjectStartByIdCommand());
        register(new ProjectStartByIndexCommand());
        register(new ProjectStartByNameCommand());
        register(new ProjectUpdateByIdCommand());
        register(new ProjectUpdateByIndexCommand());
        register(new ProjectUpdateByNameCommand());
        register(new TaskClearCommand());
        register(new TaskCreateCommand());
        register(new TaskListCommand());
        register(new TaskCompleteByIdCommand());
        register(new TaskCompleteByIndexCommand());
        register(new TaskCompleteByNameCommand());
        register(new TaskRemoveByIdCommand());
        register(new TaskRemoveByIndexCommand());
        register(new TaskRemoveByNameCommand());
        register(new TaskFindByIndexCommand());
        register(new TaskFindByIdCommand());
        register(new TaskFindByNameCommand());
        register(new TaskStartByIdCommand());
        register(new TaskStartByIndexCommand());
        register(new TaskStartByNameCommand());
        register(new TaskUpdateByIdCommand());
        register(new TaskUpdateByIndexCommand());
        register(new TaskUpdateByNameCommand());
        register(new TaskFindAllByProjectCommand());
        register(new TaskBindToProjectCommand());
        register(new TaskUnbindFromProjectCommand());
        register(new ProjectRemoveWithTasksCommand());
        register(new UserListCommand());
        register(new UserRemoveByLoginCommand());
        register(new UserRegisterCommand());
        register(new UserLoginCommand());
        register(new UserLogoutCommand());
        register(new UserShowProfileCommand());
        register(new UserChangePasswordCommand());
        register(new UserUpdateByLoginCommand());
    }

    public void register(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initUsers() {
        userService.add("test", "test", Role.USER);
        userService.add("admin", "admin", Role.ADMIN);
    }

    private void initData() {
        String userId = userService.findByLogin("test").getId();
        String projectId = projectService.add(userId, "p1", "p1").getId();
        projectService.add(userId, "p2", "p2");
        taskService.add(userId, "t1", "t1").setProjectId(projectId);
        taskService.add(userId, "t2", "t2").setProjectId(projectId);
        userId = userService.findByLogin("admin").getId();
        projectId = projectService.add(userId, "pa1", "pa1").getId();
        projectService.add(userId, "pa2", "pa2");
        taskService.add(userId, "a1", "a1").setProjectId(projectId);
        taskService.add(userId, "a2", "a2").setProjectId(projectId);
    }

    public void run(String... args) {
        initUsers();
        initData();
        displayWelcome();

        try {
            if (parseArgs(args))
                System.exit(0);
        } catch (Exception e) {
            loggerService.errors(e);
            System.exit(0);
        }
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                parseCommand(TerminalUtil.nextLine());
                System.out.println("[OK]");
            } catch (Exception e) {
                loggerService.errors(e);
                System.err.println("[FAIL]");
            } finally {
                System.out.println();
            }
        }
    }

    public void displayWelcome() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length < 1) return false;
        parseArg(args[0]);
        return true;
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        loggerService.commands(arg);
        final AbstractCommand abstractCommand = commandService.getCommandByArg(arg);
        abstractCommand.execute();
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        loggerService.commands(command);
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        abstractCommand.execute();
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}
