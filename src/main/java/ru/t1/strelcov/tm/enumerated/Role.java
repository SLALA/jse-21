package ru.t1.strelcov.tm.enumerated;

public enum Role {

    USER("User"),
    ADMIN("Administrator");

    private final String displayName;

    public static boolean isValidByName(final String name) {
        for (final Role role : values()) {
            if (role.toString().equals(name))
                return true;
        }
        return false;
    }

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
