package ru.t1.strelcov.tm.exception.empty;

import ru.t1.strelcov.tm.exception.AbstractException;

public final class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error: Id is empty.");
    }

}
