package ru.t1.strelcov.tm.model;

import ru.t1.strelcov.tm.api.entity.IWBS;
import ru.t1.strelcov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

import static ru.t1.strelcov.tm.enumerated.Status.*;

public final class Project extends AbstractBusinessEntity {

    public Project() {
        super();
    }

    public Project(String userId, String name) {
        super(userId, name);
    }

    public Project(String userId, String name, String description) {
        super(userId, name, description);
    }

}
