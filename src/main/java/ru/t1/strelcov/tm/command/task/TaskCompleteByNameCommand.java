package ru.t1.strelcov.tm.command.task;

import ru.t1.strelcov.tm.api.service.ITaskService;
import ru.t1.strelcov.tm.exception.entity.TaskNotFoundException;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.util.TerminalUtil;

import static ru.t1.strelcov.tm.enumerated.Status.COMPLETED;

public final class TaskCompleteByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-complete-by-name";
    }

    @Override
    public String description() {
        return "Complete task by name.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        final ITaskService taskService = serviceLocator.getTaskService();
        System.out.println("[COMPLETE TASK BY NAME]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.changeStatusByName(userId, name, COMPLETED);
        showTask(task);
    }

}
