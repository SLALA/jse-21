package ru.t1.strelcov.tm.command.user;

import ru.t1.strelcov.tm.api.service.IUserService;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class UserRegisterCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-register";
    }

    @Override
    public String description() {
        return "Register user.";
    }

    @Override
    public void execute() {
        final IUserService userService = serviceLocator.getUserService();
        System.out.println("[REGISTER USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        userService.add(login, password, email);
    }

}
