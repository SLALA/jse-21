package ru.t1.strelcov.tm.command;

import ru.t1.strelcov.tm.api.service.ServiceLocator;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public void setServiceLocator(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String arg();

    public abstract String name();

    public abstract String description();

    public abstract void execute();

    @Override
    public String toString() {
        String result = "";
        final String name = name();
        final String arg = arg();
        final String description = description();
        if (name != null && !name.isEmpty()) result += name + " ";
        if (arg != null && !arg.isEmpty()) result += "[" + arg + "] ";
        if (description != null && !description.isEmpty()) result += "- " + description;
        return result;
    }

}
