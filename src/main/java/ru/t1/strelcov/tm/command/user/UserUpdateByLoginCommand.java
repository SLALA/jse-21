package ru.t1.strelcov.tm.command.user;

import ru.t1.strelcov.tm.api.service.IUserService;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class UserUpdateByLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-update-by-login";
    }

    @Override
    public String description() {
        return "Update user info by login.";
    }

    @Override
    public void execute() {
        final IUserService userService = serviceLocator.getUserService();
        System.out.println("[UPDATE USER INFO]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        final User user = userService.updateByLogin(login, firstName, lastName, middleName, email);
        showUser(user);
    }

}
