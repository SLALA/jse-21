package ru.t1.strelcov.tm.command.system;

import ru.t1.strelcov.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "exit";
    }

    @Override
    public String description() {
        return "Exit program.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
