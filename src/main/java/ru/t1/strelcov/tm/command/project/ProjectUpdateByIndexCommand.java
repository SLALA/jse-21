package ru.t1.strelcov.tm.command.project;

import ru.t1.strelcov.tm.api.service.IProjectService;
import ru.t1.strelcov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-update-by-index";
    }

    @Override
    public String description() {
        return "Update project by index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        final IProjectService projectService = serviceLocator.getProjectService();
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NEW NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateByIndex(userId, index, name, description);
        showProject(project);
    }

}
