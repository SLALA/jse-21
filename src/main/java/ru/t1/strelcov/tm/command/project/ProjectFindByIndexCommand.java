package ru.t1.strelcov.tm.command.project;

import ru.t1.strelcov.tm.api.service.IProjectService;
import ru.t1.strelcov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class ProjectFindByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-find-by-index";
    }

    @Override
    public String description() {
        return "Find project by index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        final IProjectService projectService = serviceLocator.getProjectService();
        System.out.println("[FIND TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(userId, index);
        showProject(project);
    }

}
