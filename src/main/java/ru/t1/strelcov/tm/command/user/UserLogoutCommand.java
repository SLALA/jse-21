package ru.t1.strelcov.tm.command.user;

import ru.t1.strelcov.tm.api.service.IAuthService;

public final class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String description() {
        return "Logout.";
    }

    @Override
    public void execute() {
        final IAuthService authService = serviceLocator.getAuthService();
        System.out.println("[LOGOUT]");
        authService.logout();
    }

}
