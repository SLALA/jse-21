package ru.t1.strelcov.tm.command.task;

import ru.t1.strelcov.tm.command.AbstractCommand;
import ru.t1.strelcov.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(final Task task) {
        if (task == null) return;
        System.out.println("[Id]: " + task.getId());
        System.out.println("[Name]: " + task.getName());
        System.out.println("[Description]: " + task.getDescription());
        System.out.println("[Status]: " + task.getStatus().getDisplayName());
        System.out.println("[Project Id]: " + task.getProjectId());
        System.out.println("[User Id]: " + task.getUserId());
    }

}
