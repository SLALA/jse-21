package ru.t1.strelcov.tm.repository;

import ru.t1.strelcov.tm.api.repository.IUserRepository;
import ru.t1.strelcov.tm.model.User;

import java.util.Optional;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        return list.stream()
                .filter((user) -> login.equals(user.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User removeByLogin(final String login) {
        final Optional<User> user = Optional.ofNullable(findByLogin(login));
        user.ifPresent(this::remove);
        return user.orElse(null);
    }

}
