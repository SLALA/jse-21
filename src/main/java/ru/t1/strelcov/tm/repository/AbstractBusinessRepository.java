package ru.t1.strelcov.tm.repository;

import ru.t1.strelcov.tm.api.IBusinessRepository;
import ru.t1.strelcov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> implements IBusinessRepository<E> {

    @Override
    public List<E> findAll(final String userId) {
        return list.stream()
                .filter((entity) -> userId.equals(entity.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        return list.stream()
                .filter((entity) -> userId.equals(entity.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public void clear(final String userId) {
        final List<E> entities = findAll(userId);
        list.removeAll(entities);
    }

    @Override
    public E findByName(final String userId, final String name) {
        return list.stream()
                .filter((entity) -> userId.equals(entity.getUserId()) && name.equals(entity.getName()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        final List<E> entities = findAll(userId);
        return entities.stream().skip(index).findFirst().orElse(null);
    }

    @Override
    public E findById(final String userId, final String id) {
        return list.stream()
                .filter((entity) -> userId.equals(entity.getUserId()) && id.equals(entity.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public E removeById(final String userId, final String id) {
        final Optional<E> entity = Optional.ofNullable(findById(userId, id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public E removeByName(final String userId, final String name) {
        final Optional<E> entity = Optional.ofNullable(findByName(userId, name));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        final Optional<E> entity = Optional.ofNullable(findByIndex(userId, index));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

}
