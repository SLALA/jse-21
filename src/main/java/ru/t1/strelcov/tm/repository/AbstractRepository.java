package ru.t1.strelcov.tm.repository;

import ru.t1.strelcov.tm.api.IRepository;
import ru.t1.strelcov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    @Override
    public List<E> findAll() {
        return list;
    }

    @Override
    public void add(final E entity) {
        list.add(entity);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public void remove(final E entity) {
        list.remove(entity);
    }

    @Override
    public E findById(final String id) {
        return list.stream().filter((e) -> id.equals(e.getId())).findFirst().orElse(null);
    }

    @Override
    public E removeById(final String id) {
        final Optional<E> entity = Optional.ofNullable(findById(id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

}
