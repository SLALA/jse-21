package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.repository.ICommandRepository;
import ru.t1.strelcov.tm.api.service.ICommandService;
import ru.t1.strelcov.tm.command.AbstractCommand;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.system.CorruptCommandException;
import ru.t1.strelcov.tm.exception.system.UnknownCommandException;

import java.util.Collection;
import java.util.Optional;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public void add(final AbstractCommand command) {
        Optional.ofNullable(command.name()).filter((i) -> !i.isEmpty()).orElseThrow(CorruptCommandException::new);
        commandRepository.add(command);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        return Optional.ofNullable(commandRepository.getCommandByName(name)).orElseThrow(() -> new UnknownCommandException(name));
    }

    @Override
    public AbstractCommand getCommandByArg(String arg) {
        return Optional.ofNullable(commandRepository.getCommandByArg(arg)).orElseThrow(() -> new UnknownCommandException(arg));
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

}
