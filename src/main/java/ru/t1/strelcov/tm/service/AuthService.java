package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.service.IAuthService;
import ru.t1.strelcov.tm.api.service.IUserService;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.empty.EmptyLoginException;
import ru.t1.strelcov.tm.exception.empty.EmptyPasswordException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.IncorrectPasswordException;
import ru.t1.strelcov.tm.exception.entity.UserNotFoundException;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.util.HashUtil;

import java.util.Optional;

public final class AuthService implements IAuthService {

    private IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public String getUserId() {
        return Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
    }

    @Override
    public User getUser() {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        return userService.findById(userId);
    }

    @Override
    public void login(final String login, final String password) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        final User user = Optional.ofNullable(userService.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        final String passwordHash = HashUtil.salt(password);
        if (!user.getPasswordHash().equals(passwordHash)) throw new IncorrectPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        userId = null;
    }

}
