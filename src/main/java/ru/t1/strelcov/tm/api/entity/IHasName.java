package ru.t1.strelcov.tm.api.entity;

public interface IHasName {

    String getName();

    void setName(String name);

}
