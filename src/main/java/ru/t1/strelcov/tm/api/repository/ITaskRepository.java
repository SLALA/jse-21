package ru.t1.strelcov.tm.api.repository;

import ru.t1.strelcov.tm.api.IBusinessRepository;
import ru.t1.strelcov.tm.api.IRepository;
import ru.t1.strelcov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    void removeAllByProjectId(String userId, String projectId);

    List<Task> findAllByProjectId(String userId, String projectId);

}
