package ru.t1.strelcov.tm.api.service;

import ru.t1.strelcov.tm.api.IBusinessService;
import ru.t1.strelcov.tm.model.Project;

public interface IProjectService extends IBusinessService<Project> {
}
